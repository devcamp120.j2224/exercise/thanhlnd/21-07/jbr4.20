package com.devcamp.rest_api.BookAuthorAPI;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class rest_apiController {
    @GetMapping("/books")
    public ArrayList<Book> name(){
        ArrayList<Book> books = new ArrayList<Book>();
        Author author1 = new Author("Trần văn Ninh", "ninhtv@devcamp.edu.vn", 'm');
        Author author2 = new Author("Tôn nữ linh Giang", "giangtnl@devcamp.edu.vn", 'f');
        Author author3 = new Author("Lê nguyễn đức Thạnh", "thanhlnd@devcamp.edu.vn", 'm');
        Author author4 = new Author("Nguyễn hoàng Hải", "hainh@devcamp.edu.vn", 'm');

        // System.out.println(author1);
        // System.out.println(author2);
        // System.out.println(author3);
        // System.out.println(author4);

        Book book1 = new Book("Trần văn Ninh", author1, 200000, 50);
        Book book2 = new Book("Trần văn Ninh", author2, 200000, 50);
        Book book3 = new Book("Trần văn Ninh", author3, 200000, 50);
        Book book4 = new Book("Trần văn Ninh", author4, 200000, 50);

        // System.out.println(book1);
        // System.out.println(book2);
        // System.out.println(book3);
        // System.out.println(book4);

        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);

        return books;
    }
}
